
<!DOCTYPE html>
<html>
<head>
	<title>Rinkodara</title>
	<?php
	require_once 'SimpleXLSX.php';
		function draw($xl){
			$Maze = array(); $PetriukasX = 0; $PetriukasY = 0;
			if ( $xlsx = $xl) {
				$Array = $xlsx->rows();
				echo "<table style = 'border:3px solid black;'>";

				for($i = 1; $i < $Array[0][0] + 1; $i++){
					echo "<tr>";
					for($j = 0; $j < $Array[0][1]; ++$j){
						echo "<td style = 'border:2px solid red; width:20px;height:20px;'>".$Array[$i][$j]."</td>";
						if($Array[$i][$j] == "P"){
							$PetriukasX = $i - 1;
							$PetriukasY = $j;
							$Maze[$i - 1][$j] = 2;
						}
						elseif($Array[$i][$j] == "E"){
							$Maze[$i - 1][$j] = 3;
						}
						elseif($Array[$i][$j] == "M"){
							$Maze[$i - 1][$j] = 0;
						}
						else{
							$Maze[$i - 1][$j] = 1;
						}
					}
					echo "</tr>";
				}
				echo "</table>";
			} 
			else {
					echo "Error in loading .xlsx";
			}
			echo "<script> var maze =".json_encode($Maze).";var startX =".$PetriukasX.";var startY =".$PetriukasY."</script>";
		}
	?>
	<script type ="text/javascript">
		function MazeSolver(maze) {
	    	this.maze = maze;
	    	this.traverse = function(column, row) {
	        	if(this.maze[column][row] == 3) {
		            document.writeln("Solved at (" + column + ", " + row + ")</br>");
		           	return ;
		        } 
		        else if(this.maze[column][row] == 1 || this.maze[column][row] == 2) {
		            document.writeln("currently at (" + column + ", " + row + ")</br>");
		            this.maze[column][row] = 9;
		            if(column < this.maze.length - 1) {
		                this.traverse(column + 1, row);
		            }
		            if(row < this.maze[column].length - 1) {
		                this.traverse(column, row + 1);
		            }
		            if(column > 0) {
		                this.traverse(column - 1, row);
		            }
		            if(row > 0) {
		                this.traverse(column, row - 1);
		            }
	        	}
    		};
		};
		function execute(){
			var ms = new MazeSolver(maze);
			ms.traverse(startX, startY);

		}
	</script>
</head>

<body>	
	<div style = "float:right;min-width:60%;">
		<h3>Maze</h3>
		<form action = <?php echo $_SERVER['PHP_SELF'];?> method ="GET">
			<button name = "small">Small board</button>
			<button name = "big">Big board</button>
		</form>
		<?php
			if(isset($_GET['small'])){
				draw(SimpleXLSX::parse('mazas.xlsx'));
				echo "<script> execute(); </script>";
			}
			if(isset($_GET['big'])){
				draw(SimpleXLSX::parse('zigzagas.xlsx'));
				echo "<script> execute(); </script>";
			}
		?>
	</div>
</body>
</html>